#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "WARNING!!! This script will *destroy everything* on /dev/vda"
echo "Do NOT use this script if you need to do manual partitioning!"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

mkfs.vfat -F32 /dev/vda1

mkfs.ext2 /dev/vda2

mkfs.btrfs -f -L ROOT /dev/vda3

mount /dev/vda3 /mnt

btrfs su cr /mnt/@
btrfs su cr /mnt/@home
btrfs su cr /mnt/@var
btrfs su cr /mnt/@pkg
btrfs su cr /mnt/@snapshots

umount /mnt

mount -o noatime,ssd,compress=zstd,subvol=@ /dev/vda3 /mnt

mkdir /mnt/home
mkdir /mnt/var
mkdir /mnt/.snapshots

mount -o noatime,ssd,compress=zstd,subvol=@home /dev/vda3 /mnt/home
mount -o noatime,ssd,compress=zstd,subvol=@var /dev/vda3 /mnt/var

mkdir /mnt/var/cache
mkdir /mnt/var/cache/pacman
mkdir /mnt/var/cache/pacman/pkg

mount -o noatime,ssd,compress=zstd,subvol=@pkg /dev/vda3 /mnt/var/cache/pacman/pkg
mount -o noatime,ssd,compress=zstd,subvol=@snapshots /dev/vda3 /mnt/.snapshots

mkdir /mnt/boot

mount /dev/vda2 /mnt/boot

mkdir /mnt/boot/efi

mount /dev/vda1 /mnt/boot/efi

echo
echo "If you can read this, you don't need glasses."
echo "Also, the format and mount script finished correctly."
echo
mount | grep /mnt
