#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "WARNING!!! This script will *destroy everything* on /dev/vda"
echo "Do NOT use this script if you need to do manual partitioning!"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

mkfs.vfat -F32 /dev/vda1

mkfs.ext2 /dev/vda2

mkfs.ext4 -F -L root /dev/vda3

mount /dev/vda3 /mnt

mkdir /mnt/boot

mount /dev/vda2 /mnt/boot

mkdir /mnt/boot/efi

mount /dev/vda1 /mnt/boot/efi

echo
echo "If you can read this, you don't need glasses."
echo "Also, the format and mount script finished correctly."
echo
mount | grep /mnt
