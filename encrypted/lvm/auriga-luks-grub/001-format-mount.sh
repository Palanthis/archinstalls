#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "WARNING!!! This script will *destroy everything* on nvme0n1"
echo "Do NOT use this script if you need to do manual partitioning!"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

mkfs.vfat -F32 /dev/nvme0n1p1

mkfs.ext2 /dev/nvme0n1p2

cryptsetup -c aes-xts-plain64 -y --use-random luksFormat /dev/nvme0n1p3

cryptsetup luksOpen /dev/nvme0n1p3 luks

pvcreate /dev/mapper/luks

vgcreate vg0 /dev/mapper/luks

lvcreate --size 4G vg0 --name swap

lvcreate -l +100%FREE vg0 --name root

mkfs.ext4 /dev/mapper/vg0-root

mkswap /dev/mapper/vg0-swap

mount /dev/mapper/vg0-root /mnt

swapon /dev/mapper/vg0-swap

mkdir /mnt/boot

mount /dev/nvme0n1p2 /mnt/boot

mkdir /mnt/boot/efi

mount /dev/nvme0n1p1 /mnt/boot/efi

echo
echo "Do these mount points look right?"
echo
mount | grep /mnt
echo
echo
echo "Here is the UUID for your root parition."
blkid | grep /dev/nvme0n1p3
echo
