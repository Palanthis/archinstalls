#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "WARNING!!! This script will *destroy everything* on /dev/nvme0n1"
echo "Do NOT use this script if you need to do manual partitioning!"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

sfdisk /dev/nvme0n1 < nvme0n1-uefi.sfdisk

read -n 1 -s -r -p "/dev/nvme0n1 done. Press Enter to continue or Ctrl+C to quit."

fdisk -l | grep /dev/nvme0n1

echo
echo "Do these partitions look okay?"
echo
read -n 1 -s -r -p "Press Enter to continue."


