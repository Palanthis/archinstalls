#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "WARNING!!! This script will *destroy everything* on /dev/sda"
echo "Do NOT use this script if you need to do manual partitioning!"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

mkfs.vfat -F32 /dev/sda1

mkfs.ext2 /dev/sda2

cryptsetup -c aes-xts-plain64 -y --use-random luksFormat /dev/sda3

cryptsetup luksOpen /dev/sda3 crypt_root

mkfs.btrfs -f -L ROOT /dev/mapper/crypt_root

# btrfs stuff will go here

mount /dev/mapper/crypt_root /mnt

# Here is where we will create subvolumes.
# We will then dismount /mnt and remount the
# subvlumes before continuing on to pacstrap

btrfs su cr /mnt/@
btrfs su cr /mnt/@home
btrfs su cr /mnt/@var
btrfs su cr /mnt/@pkg
btrfs su cr /mnt/@snapshots

umount /mnt

mount -o noatime,compress=zstd,ssd,subvol=@ /dev/mapper/crypt_root /mnt
mkdir /mnt/home
mkdir /mnt/var
mkdir /mnt/.snapshots

mount -o noatime,compress=zstd,ssd,subvol=@home /dev/mapper/crypt_root /mnt/home
mount -o noatime,compress=zstd,ssd,subvol=@var /dev/mapper/crypt_root /mnt/var
mkdir /mnt/var/cache
mkdir /mnt/var/cache/pacman
mkdir /mnt/var/cache/pacman/pkg
mount -o noatime,compress=zstd,ssd,subvol=@pkg /dev/mapper/crypt_root /mnt/var/cache/pacman/pkg
mount -o noatime,compress=zstd,ssd,subvol=@snapshots /dev/mapper/crypt_root /mnt/.snapshots

mkdir /mnt/boot

mount /dev/sda2 /mnt/boot

mkdir /mnt/boot/efi

mount /dev/sda1 /mnt/boot/efi

echo "Do these subvolumes look right?"
mount | grep btrfs
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

echo
echo "Here is the UUID for your root parition. Add this to post/luks_grub file"
blkid | grep /dev/sda3
echo
