#!/bin/bash
# Written by Palanthis
set -e
 
# Change bs= to the value in MB you want for the swapfile
dd if=/dev/zero of=/swapfile bs=4096 count=1048576

chmod 600 /swapfile

mkswap /swapfile

swapon /swapfile

echo "/swapfile none swap defaults 0 0" >> /etc/fstab

swapoff /swapfile