#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "WARNING!!! This script will *destroy everything* on /dev/sda"
echo "Do NOT use this script if you need to do manual partitioning!"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

mkfs.vfat -F32 /dev/sda1

mkfs.ext2 /dev/sda2

cryptsetup -c aes-xts-plain64 -y --use-random luksFormat /dev/sda3

cryptsetup luksOpen /dev/sda3 crypt_root

mkfs.ext4 -F -L ROOT /dev/mapper/crypt_root

mount /dev/mapper/crypt_root /mnt

mkdir /mnt/boot

mount /dev/sda2 /mnt/boot

mkdir /mnt/boot/efi

mount /dev/sda1 /mnt/boot/efi

echo
echo "Here is the UUID for your root parition. Add this to post/luks_grub file"
blkid | grep /dev/sda3
echo
